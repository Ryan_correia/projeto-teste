#!/bin/bash

#função para rodar a poesia


rodar_poesia() {
	echo "$1"
	sleep 2 
}

poesia=(
    "Em Quahog, cidade peculiar e sem igual,"
    "Onde Peter, o patriarca, sempre quer causar,"
    "Com sua família em cada aventura bizarra,"
    "Nenhum momento é comum, cada um é uma raridade bizarra."

)

for verso in "${poesia[@]}"; do
	rodar_poesia "$verso"
done


